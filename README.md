# OpenML dataset: algae

https://www.openml.org/d/45755

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains 340 instances concerning the frequencies of seven types of algae populations in different environments.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45755) of an [OpenML dataset](https://www.openml.org/d/45755). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45755/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45755/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45755/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

